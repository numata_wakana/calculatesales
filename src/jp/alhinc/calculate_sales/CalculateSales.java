package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_CONTINUE = "売上ファイル名が連番になっていません";
	private static final String TOTALAMOUNT_OVER = "合計金額が10桁を超えました";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */

	public static void main(String[] args) {

		//エラー処理3-1(コマンドライン引数が渡されているか確認）
		if (args.length !=1) {
			System.out.println("UNKNOWN_ERROR");

			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales)) {
			return;
		}

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i =0; i< files.length ; i++) {

			if(files[i].getName().matches("^[0-9]{8}\\.rcd$")) {

				rcdFiles.add(files[i]);
			}
		}
		//エラー処理2-1(売上ファイル名が連番であるか確認）
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() -1; i++) {

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

			if((latter - former) != 1) {

				System.out.println("FILE_NOT_CONTINUE");
				return;

			}
		}

		BufferedReader br = null;

		for(int i =0; i< rcdFiles.size() ; i++) {

			try {

				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				List<String> fileContents = new ArrayList<>();

				String line;
				while((line = br.readLine()) != null) {

					fileContents.add(line);
				}
				//エラー処理2-4(売上ファイルの中身が2行であるか確認）
				if(fileContents.size() !=2 ) {

					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");

					return;
				}
				//エラー処理3-2(売上額は数字であるか確認）
				if(! fileContents.get(1).matches("^[0-9]*$")) {

					System.out.println("UNKNOWN_ERROR");

					return;
				}
				//エラー処理2-3(支店コードが一致しているか確認）
				if (! branchNames.containsKey(branchSales)) {

					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");

					return;
				}

				long fileSale = Long.parseLong(fileContents.get(1));
				Long saleAmount = branchSales.get(fileContents.get(0))+ fileSale;

				//エラー処理2-2(売上金額が10桁以上でないか確認
				if(saleAmount >= 10000000000L) {

					System.out.println("TOTALAMOUNT_OVER");

					return;
				}
				branchSales.put(fileContents.get(0) , fileSale);

			} catch(IOException e) {

				System.out.println(UNKNOWN_ERROR);

				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {

						System.out.println(UNKNOWN_ERROR);

						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
	}
	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		BufferedReader br = null;

		try {

			File file = new File(path, fileName);

			//エラー処理1-1(支店定義ファイルが存在するか確認）
			if(!file.exists()) {

				System.out.println(FILE_NOT_EXIST);

				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			while((line = br.readLine()) != null) {

				String[]items = line.split(",");

				//エラー処理1-2(フォーマットが正しいか確認）
				if(( items.length !=2 )||(! items[0].matches("^[0-9]{3}$"))){

					System.out.println(FILE_INVALID_FORMAT);

					return false;
				}
				branchNames.put(items[0],items[1]);
				branchSales.put(items[0],0L);
			}
		} catch(IOException e) {

			System.out.println(UNKNOWN_ERROR);

			return false;
		} finally {

			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {

					System.out.println(UNKNOWN_ERROR);

					return false;
				}
			}
		}
		return true;
	}
	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		BufferedWriter bw = null;

		try {

			File file = new File(path,fileName);
			FileWriter filewriter = new FileWriter(file);
			bw = new BufferedWriter(filewriter);

			for(String key : branchNames.keySet()) {

				bw.write(key + ","+ branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {

			System.out.println(UNKNOWN_ERROR);

			return false;

		} finally {

			if(bw != null) {
				try {
					bw.close();

				} catch(IOException e) {

					System.out.println(UNKNOWN_ERROR);

					return false;
				}
			}
		}
		return true;
	}
}
